import {jsonFetch} from '../functions/api';
import {BASE_URL} from '../functions/config.js';

/**
 * Représentation d'un magasin de l'API
 * @typedef {{
 *  id: number,
 *  name: string
 * }} WarehouseResource
 *
 *
 * @typedef {{
 *  id: number,
 *  label: string
 * }} AlertTypeResource
 *
 */

/**
 * @return {Promise<WarehouseResource[]>}
 */
export async function findAllWarehouses() {
  return await jsonFetch(`${BASE_URL}/ware_houses`);
}

/**
 * @return {Promise<AlertTypeResource[]>}
 */
export async function findAllAlertTypes() {
  return await jsonFetch(`${BASE_URL}/alert_types`);
}
