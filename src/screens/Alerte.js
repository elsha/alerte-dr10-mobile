import React, { useEffect, useLayoutEffect, useState } from 'react'
import { Text, View, ImageBackground, Image, TouchableOpacity, TextInput } from 'react-native';
import bg from "../../images/7.png";
import bell from "../../images/bell-dark.png";
import { useNavigation } from '@react-navigation/native';
import * as theme from '../../theme'
import SelectDropdown from 'react-native-select-dropdown';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { jsonFetch } from '../functions/api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import { API_URL } from '../../config';
import { AlertNotificationRoot, ALERT_TYPE, Dialog } from 'react-native-alert-notification';

const Alerte = () => {
  const navigation = useNavigation();
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false
    });
  }, [])

  const [alerts, setAlerts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [alert, setAlert] = useState({
    id: null,
    employee: '',
    content: '',
    alertType: ''
  })
  const [error, setError] = useState(null)

  // On recupère les types d'alerte l'affichage du composant
  useEffect(() => {
    async function findAllAlertTypes() {
      const alerts = await jsonFetch(`${API_URL}/alert_types`)
      setAlerts(alerts)
      setLoading(false)
    }
    findAllAlertTypes();
    getEmployee()
  }, [loading])

  const getEmployee = async () => {
    try {
      await AsyncStorage.getItem("employee")
        .then((response) => {
          const currentEmployee = JSON.parse(response);
          if (currentEmployee !== null) {
            let a = { ...alert }
            a['employee'] = `/api/employees/${currentEmployee.id}`;
            setAlert(a)
          } else {
            navigation.navigate('Home')
          }
        });
    } catch (error) {
      console.log(error);
    }
  }

  const handleChange = (name, value, isSelect = false) => {
    let a = { ...alert }
    if (isSelect) {
      a[name] = `/api/alert_types/${value}`;
    } else {
      a[name] = value;
    }
    setAlert(a)
  }

  const handleSubmit = () => {
    setError(alert.alertType != "" ? null : "Vous devez choisir un type d'alerte");
    if (error === null) {
      async function addAlert() {
        setLoading(true)
        try {
          await jsonFetch(
            `${API_URL}/alerts`,
            {
              method: 'POST',
              body: alert
            }
          )
        } catch (error) {
          if (error instanceof ApiError) {
            setError(error.violationsFor('alertType'))
          }
        }
        setLoading(false)
        showDialog("Votre alerte a été envoyé avec succès.")
      }
      addAlert();
    }
  }

  const showDialog = (message) => {
    Dialog.show({
      type: ALERT_TYPE.SUCCESS,
      title: 'Success',
      textBody: message,
      button: 'Fermer',
      onHide: () => { navigation.replace('Home') }
    })
  }

  return (
    <View className="flex-1">
      <ImageBackground source={bg} resizeMode="stretch" className="flex-1 justify-center">
        <Spinner
          visible={loading}
          textContent={'Chargement...'}
          className="text-white"
          animation="fade"
        />
        <AlertNotificationRoot>
          <View className="items-center mt-12">
            <Image source={bell} className="h-44 w-44" />
            <Text className="font-extrabold text-xl" style={[theme.styles.text_blue, theme.styles.h1]}>Créer un alerte</Text>
            <View className="mt-3">
              <SelectDropdown
                data={alerts}
                onSelect={(selectedItem, index) => {
                  if (selectedItem) {
                    handleChange('alertType', selectedItem.id, true)
                  }
                }}
                defaultButtonText={"Choisir le type d'alerte"}
                buttonTextAfterSelection={(selectedItem, index) => {
                  return (<Text>{selectedItem ? selectedItem.label : "Choisir le type d'alerte"}</Text>);
                }}
                rowTextForSelection={(item, index) => {
                  return (<Text>{item.label}</Text>);
                }}
                buttonStyle={theme.styles.dropdown1BtnStyle}
                buttonTextStyle={theme.styles.dropdown1BtnTxtStyle}
                renderDropdownIcon={isOpened => {
                  return <FontAwesome
                    name={isOpened ? 'chevron-up' : 'chevron-down'} color={theme.colors.blue} size={16} />;
                }}
                dropdownIconPosition={'right'}
                dropdownStyle={theme.styles.dropdown1DropdownStyle}
                rowStyle={theme.styles.dropdown1RowStyle}
                rowTextStyle={theme.styles.dropdown1RowTxtStyle}
                selectedRowStyle={theme.styles.dropdown1SelectedRowStyle}
                search
                searchInputStyle={theme.styles.dropdown1searchInputStyleStyle}
                searchPlaceHolder={'Faire une recherche ici'}
                searchPlaceHolderColor={theme.colors.blue}
                renderSearchInputLeftIcon={() => {
                  return <FontAwesome name={'search'} color={theme.colors.blue} size={16} />;
                }}
              />
              {error && <Text className="text-red-600 text-xs">{error}</Text>}

              <TextInput className="bg-white rounded-2xl w-72 pl-3 mt-4"
                style={{ borderTopWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, borderBottomColor: theme.colors.blue, borderBottomWidth: 4 }}
                placeholder="Commentaire"
                maxLength={200}
                multiline={true}
                numberOfLines={3}
                textAlignVertical='top'
                value={alert.content ? alert.content : ''}
                onChangeText={value => handleChange('content', value)}
                underlineColorAndroid="transparent"
                placeholderTextColor={theme.colors.blue}
              />
            </View>
          </View>
          <View className="flex-1 items-center">
            <View className="mt-10 rounded-xl w-44" style={[theme.styles.bg_blue, theme.fonts.h1]}>
              <TouchableOpacity
                onPress={handleSubmit}
                className="p-2 mr-4 rounded-xl"
                style={theme.styles.bg_yellow}>
                <View className="flex-row justify-center items-center">
                  <Text className=" font-extrabold" style={theme.styles.text_blue}>Enregistrer</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </AlertNotificationRoot>
      </ImageBackground>
    </View>
  )
}

export default Alerte