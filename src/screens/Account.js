import React, { useLayoutEffect, useState, useEffect } from 'react'
import { Text, View, ImageBackground, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import { useNavigation } from '@react-navigation/native'
import SelectDropdown from 'react-native-select-dropdown'
import bg from "../../images/bg2.png";
import * as theme from "../../theme"
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { ApiError, jsonFetch } from '../functions/api'
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { API_URL } from '../../config';
import { AlertNotificationRoot, ALERT_TYPE, Dialog } from 'react-native-alert-notification';

const Account = () => {

  const navigation = useNavigation();
  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false
    });
  }, [])

  const [employee, setEmployee] = useState({
    id: null,
    name: '',
    phone: '',
    phoneReference: '',
    wareHouse: ''
  })
  const [errors, setErrors] = useState({
    phone: null,
    name: null,
    wareHouse: null
  })
  const [warehouses, setWarehouses] = useState([]);
  const [warehouse, setWarehouse] = useState(null);
  const [loading, setLoading] = useState(true);
  const [index, setIndex] = useState(true);

  // On recupère les magasins l'affichage du composant
  useEffect(() => {
    async function findAllWarehouses() {
      const warehouses = await jsonFetch(`${API_URL}/ware_houses`)
      setWarehouses(warehouses)
      setLoading(false)
    }
    findAllWarehouses();
    getEmployee();
  }, [loading])

  const handleChange = (name, value, isSelect = false) => {
    let e = { ...employee }
    if (isSelect) {
      e[name] = `/api/ware_houses/${value}`;
    } else {
      e[name] = value;
      if (name === 'phone') {
        e['phoneReference'] = value;
      }
    }
    setEmployee(e)
  }

  const handleSubmit = () => {
    let er = { ...errors }
    er['name'] = employee.name != "" ? null : "Votre nom est obligatoire";
    er['phone'] = employee.phone != "" ? null : "Le numéro de téléphone est obligatoire";
    er['wareHouse'] = employee.wareHouse != "" ? null : "Vous devez choisir au moins un magasin";
    setErrors(er);
    if (errors.name === null && errors.phone === null && errors.wareHouse === null) {
      setLoading(true)
      async function addEmployee() {
        try {
          if (employee.id === null) {
            const newEmployee = await jsonFetch(
              `${API_URL}/employees`,
              {
                method: 'POST',
                body: employee
              }
            )
            storeEmployee(newEmployee)
            showDialog('Félicitations! Vos informations ont été enregistrées avec succès. Vous pouvez desormais créer vos alertes.')
          } else {
            const newEmployee = await jsonFetch(
              `${API_URL}/employees/${employee.id}`,
              {
                method: 'PUT',
                body: employee
              }
            )
            storeEmployee(newEmployee)
            getEmployee()
            showDialog("Félicitations! Vos informations ont été mise à jours avec succès.")
          }
        } catch (error) {
          if (error instanceof ApiError) {
            console.log(error.violationsFor('phone'))
            er['name'] = error.violationsFor('name');
            er['phone'] = error.violationsFor('phone')
            setErrors(er)
          }
        }
        setLoading(false)
      }
      addEmployee();
    }
  }

  const getEmployee = async () => {
    try {
      await AsyncStorage.getItem("employee")
        .then((response) => {
          const currentEmployee = JSON.parse(response);
          if (currentEmployee != null) {
            setEmployee(currentEmployee)
            if (currentEmployee.wareHouse != "") {
              const id = parseInt(currentEmployee.wareHouse.split('/api/ware_houses/')[1], 10)
              const defaultWarehouse = warehouses.find(warehouse => warehouse.id === id)
              const currentIndex = warehouses.findIndex(warehouse => warehouse.id === id)
              setWarehouse(defaultWarehouse)
              setIndex(currentIndex !== -1 ? currentIndex : null)
            }
          }

        });
    } catch (error) {
      console.log(error);
    }
  };

  // storing data
  const storeEmployee = async (value) => {
    try {
      await AsyncStorage.setItem("employee", JSON.stringify(value));
    } catch (error) {
      console.log(error);
    }
  };

  const showDialog = (message) => {
    Dialog.show({
      type: ALERT_TYPE.SUCCESS,
      title: 'Success',
      textBody: message,
      button: 'Fermer',
      onHide: () => { navigation.replace('Home')}
    })
  }

  return (
    <View className="flex-1">
      <ImageBackground source={bg} resizeMode="stretch" className="flex-1 justify-center h-auto">
        <AlertNotificationRoot>
          <Spinner
            visible={loading}
            textContent={'Chargement...'}
            className="text-white"
            animation="fade"
          />
          <View className="flex-1 justify-center items-center">
            <Text className="font-extrabold" style={[theme.styles.text_blue, { fontSize: 22 }]}>Configurer mon compte</Text>
            <View className="mt-10">
              <TextInput className="bg-white rounded-2xl w-72 pl-3"
                style={{ borderBottomColor: theme.colors.blue, borderBottomWidth: 2.5 }}
                placeholder="Nom et Prénom"
                maxLength={30}
                value={employee.name ? employee.name : ''}
                onChangeText={value => handleChange('name', value)}
                underlineColorAndroid="transparent"
                placeholderTextColor={theme.colors.blue}
              />
              {errors.name && <Text className="text-red-600  text-xs">{errors.name}</Text>}
              <TextInput className="bg-white rounded-2xl w-72 pl-3 mt-3"
                style={{ borderBottomColor: theme.colors.blue, borderBottomWidth: 2.5 }}
                placeholder="Téléphone"
                keyboardType="number-pad"
                maxLength={10}
                value={employee.phone ? employee.phone : ''}
                onChangeText={value => handleChange('phone', value)}
                underlineColorAndroid="transparent"
                placeholderTextColor={theme.colors.blue}
              />
              {errors.phone && <Text className="text-red-600 text-xs">{errors.phone}</Text>}
            </View>
            <View className="mt-3">
              <SelectDropdown
                data={warehouses}
                defaultValueByIndex={index}
                defaultValue={warehouse}
                onSelect={(selectedItem, index) => {
                  if (selectedItem) {
                    handleChange('wareHouse', selectedItem.id, true)
                  }
                }}
                defaultButtonText={'Choisir le magasin'}
                buttonTextAfterSelection={(selectedItem, index) => {
                  return (<Text>{selectedItem ? selectedItem.name : 'Choisir le magasin'}</Text>);
                }}
                rowTextForSelection={(item, index) => {
                  return (<Text>{item.name}</Text>);
                }}
                buttonStyle={styles.dropdown1BtnStyle}
                buttonTextStyle={styles.dropdown1BtnTxtStyle}
                renderDropdownIcon={isOpened => {
                  return <FontAwesome
                    name={isOpened ? 'chevron-up' : 'chevron-down'} color={theme.colors.blue} size={16} />;
                }}
                dropdownIconPosition={'right'}
                dropdownStyle={styles.dropdown1DropdownStyle}
                rowStyle={styles.dropdown1RowStyle}
                rowTextStyle={styles.dropdown1RowTxtStyle}
                selectedRowStyle={styles.dropdown1SelectedRowStyle}
                search
                searchInputStyle={styles.dropdown1searchInputStyleStyle}
                searchPlaceHolder={'Faire une recherche ici'}
                searchPlaceHolderColor={theme.colors.blue}
                renderSearchInputLeftIcon={() => {
                  return <FontAwesome name={'search'} color={theme.colors.blue} size={16} />;
                }}
              />
              {errors.wareHouse && <Text className="text-red-600 text-xs">{errors.wareHouse}</Text>}
            </View>
            <View className="mt-10 rounded-xl w-44" style={[theme.styles.bg_blue, theme.fonts.h1]}>
              <TouchableOpacity
                onPress={handleSubmit}
                className="p-2 mr-4 rounded-xl"
                style={theme.styles.bg_yellow}>
                <View className="flex-row justify-center items-center">
                  <Text className=" font-extrabold" style={theme.styles.text_blue}>Enregistrer</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </AlertNotificationRoot>
      </ImageBackground>
    </View>
  )
}

const styles = StyleSheet.create({

  dropdown1BtnStyle: {
    width: '70%',
    height: 50,
    backgroundColor: '#FFF',
    borderRadius: 12,
    borderBottomWidth: 2.5,
    borderBottomColor: theme.colors.blue
  },
  dropdown1BtnTxtStyle: {
    color: theme.colors.blue,
    fontSize: 14,
    textAlign: 'left',
  },
  dropdown1DropdownStyle: {
    backgroundColor: '#EFEFEF'
  },
  dropdown1RowStyle: {
    backgroundColor: '#EFEFEF',
    borderBottomColor: theme.colors.blue
  },
  dropdown1RowTxtStyle: {
    color: theme.colors.blue,
    textAlign: 'left'
  },
  dropdown1SelectedRowStyle: {
    backgroundColor: 'rgba(0,0,0,0.1)'
  },
  dropdown1searchInputStyleStyle: {
    backgroundColor: '#EFEFEF',
    borderRadius: 8,
    borderBottomWidth: 1,
    borderBottomColor: '#444',
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#FFF",
    height: 200,
    width: '80%',
    borderRadius: 10,
    borderWidth: 2,
    borderColor: theme.colors.blue,
    marginTop: 80,
    marginLeft: 40,

  }
});

export default Account