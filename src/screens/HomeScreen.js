import React, { useEffect, useLayoutEffect, useState } from 'react'
import { Text, View, ImageBackground, Image, TouchableOpacity, Alert, Modal, StyleSheet, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native'
import bg from "../../images/bg1.png";
import logo from "../../images/logo.png";
import bell from "../../images/bell-dark.png";
import account from "../../images/account.png";
import phone from "../../images/phone-dark.png";
import * as theme from "../../theme"
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ApiError, jsonFetch } from '../functions/api';
import API_URL from '../../config'
import { ALERT_TYPE, AlertNotificationRoot, Toast } from 'react-native-alert-notification';

const HomeScreen = ({ route }) => {

  const navigation = useNavigation();

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false
    });
  }, [])

  const [loading, setLoading] = useState(true);
  const [employee, setEmployee] = useState(null)
  const [modalVisible, setModalVisible] = useState(false);
  const [request, setRequest] = useState({ employee: '' })

  // On teste si l'employé a déjà ses informations renseigné à la base
  useEffect(() => {
    // AsyncStorage.clear()
    getEmployee()
    setLoading(false)
  }, [loading, route])

  const getEmployee = async () => {
    try {
      await AsyncStorage.getItem("employee")
        .then((response) => {
          const currentEmployee = JSON.parse(response);
          if (currentEmployee !== null) {
            let r = { ...request }
            r['employee'] = `/api/employees/${currentEmployee.id}`;
            setRequest(r)
            setEmployee(currentEmployee)
          }
        });
    } catch (error) {
      console.log(error);
    }
  }

  const handleSubmit = () => {
    async function callRequest() {
      console.log(request)
      setLoading(true)
      try {
        await jsonFetch(
          `${API_URL}/call_requests`,
          {
            method: 'POST',
            body: request
          }
        )
        
      } catch (error) {
        if (error instanceof ApiError) {
          console(error)
        }
      }
      setLoading(false)
      setModalVisible(false)
      showToast()
    }
    callRequest();
  }

  const showToast = () => {
    Toast.show({
      type: ALERT_TYPE.SUCCESS,
      title: 'Success',
      textBody: 'Votre demande à été envoyé avec succès',
    })
  }

  return (
    <View className="flex-1">
      <ImageBackground source={bg} resizeMode="stretch" className="flex-1 justify-center">
        <Spinner
          visible={loading}
          textContent={'Chargement...'}
          className="text-white"
          animation="fade"
        />
        <AlertNotificationRoot>
          <View className="flex-1 justify-center items-center">
            <Image source={logo} />
            <Image source={bell} className="h-36 w-36" />
            {employee && <View className="mt-2 rounded-3xl w-48" style={theme.styles.bg_blue}>
              <TouchableOpacity className="pr-2 pl-2 mr-4 rounded-3xl" style={theme.styles.bg_yellow}
                onPress={() => navigation.navigate('Alerte')}
              >
                <View className="flex-row justify-center items-center">
                  <Image source={bell} className="h-10 w-10" />
                  <Text className="font-extrabold" style={theme.styles.text_blue}>Créer une alerte</Text>
                </View>
              </TouchableOpacity>
            </View>}
            {employee && <View className="mt-3 rounded-3xl w-48" style={theme.styles.bg_blue}>
              <TouchableOpacity className="pr-2 pl-1 mr-4 rounded-3xl" style={theme.styles.bg_yellow}
                onPress={() => {
                  setModalVisible(true);
                }}
              >
                <View className="flex-row justify-center items-center">
                  <Image source={phone} className="h-10 w-10 mr-4" />
                  <Text className="font-extrabold" style={theme.styles.text_blue}>Être rappelé</Text>
                </View>
              </TouchableOpacity>
            </View>}
            {!employee && !loading && <View className="mt-10 rounded-3xl" style={theme.styles.bg_blue}>
              <TouchableOpacity className="pr-2 pl-2 ml-4 rounded-3xl"
                style={theme.styles.bg_yellow}
                onPress={() => navigation.navigate('Account')}
              >
                <View className="flex-row justify-center items-center">
                  <Image source={account} className="h-10 w-10" />
                  <Text className="font-extrabold" style={theme.styles.text_blue}>Configurer mon compte</Text>
                </View>
              </TouchableOpacity>
            </View>}
          </View>
          {employee && <View className="justify-end items-end">
            <View className="rounded-3xl w-48" style={theme.styles.bg_blue}>
              <TouchableOpacity className="pr-1 ml-4 rounded-3xl"
                style={theme.styles.bg_yellow}
                onPress={() => navigation.navigate('Account')}
              >
                <View className="flex-row justify-center items-center">
                  <Image source={account} className="h-10 w-10" />
                  <Text className="font-extrabold" style={theme.styles.text_blue}>Mon Compte</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>}
          <Modal
            transparent={false}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
            <View style={styles.modal}>
              <View>
                <View style={styles.modalContainer}>
                  {/* Modal Header */}
                  <View style={styles.modalHeader}>
                    <Text style={styles.title}>Demander à être rappelé</Text>
                    <View style={styles.divider}></View>
                  </View>
                  {/* Modal Body */}
                  <View style={styles.modalBody}>
                    <Text style={styles.bodyText}>Êtes-vous sûr de vouloir être rappelé ?</Text>
                  </View>
                  {/* Modal Footer */}
                  <View style={styles.modalFooter}>
                    <View style={styles.divider}></View>
                    <View style={{ flexDirection: "row-reverse", margin: 10 }}>
                      <TouchableOpacity style={{ ...styles.actions, backgroundColor: "#db2828" }}
                        onPress={() => {
                          setModalVisible(!modalVisible);
                        }}>
                        <Text style={styles.actionText}>Non</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={handleSubmit}
                        style={[{ ...styles.actions }, theme.styles.bg_blue]}>
                        <Text style={styles.actionText}>Oui</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </Modal>
        </AlertNotificationRoot>
      </ImageBackground>
    </View>
  )
}
const styles = StyleSheet.create({
  modal: {
    backgroundColor: "lightgray",
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalContainer: {
    backgroundColor: "#f9fafb",
    width: "80%",
    borderRadius: 15,
    borderColor: theme.colors.yellow,
    borderWidth: 1
  },
  title: {
    fontWeight: "bold",
    fontSize: 16,
    padding: 15,
    color: theme.colors.blue
  },
  divider: {
    width: "100%",
    height: 1,
    backgroundColor: "lightgray"
  },
  modalBody: {
    backgroundColor: "#fff",
    paddingVertical: 20,
    paddingHorizontal: 10
  },
  actions: {
    borderRadius: 5,
    marginHorizontal: 10,
    paddingVertical: 5,
    paddingHorizontal: 15
  },
  actionText: {
    color: "#fff"
  }
});

export default HomeScreen

