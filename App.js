import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from './src/screens/HomeScreen';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Account from './src/screens/Account';
import Alerte from './src/screens/Alerte';

const Stack = createNativeStackNavigator();


export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Account" component={Account} />
        <Stack.Screen name="Alerte" component={Alerte} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};