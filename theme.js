import { StyleSheet } from 'react-native';

const colors = {
  blue: "#0F248D",
  yellow: "#FDF904"
};

const sizes = {
  // global sizes
  base: 12,
  font: 12,
  border: 10,

  // font sizes
  h1: 32,
  h2: 26,
  h3: 18,
  title: 16,
  body: 12,
  caption: 12,
  small: 8,
  medium: 10
};

const fonts = {
  h1: {
    // fontFamily: "Rubik-Bold",
    fontSize: sizes.h1
  },
  h2: {
    fontFamily: "Rubik-Bold",
    fontSize: sizes.h2
  },
  h3: {
    fontFamily: "Rubik-Bold",
    fontSize: sizes.h3
  },
  title: {
    fontFamily: "Rubik-Bold",
    fontSize: sizes.title
  },
  body: {
    fontSize: sizes.body
  },
  caption: {
    fontSize: sizes.caption
  },
  small: {
    fontSize: sizes.small
  },
  medium: {
    fontSize: sizes.medium
  }
};

const styles = StyleSheet.create({
  bg_blue: {
    backgroundColor: colors.blue
  },
  text_blue: {
    color: colors.blue,
    fontSize: 16
  },
  h1: {
    fontSize: 20
  },
  bg_yellow: {
    backgroundColor: colors.yellow
  },
  dropdown1BtnStyle: {
    width: '70%',
    height: 50,
    backgroundColor: '#FFF',
    borderRadius: 12,
    borderBottomWidth: 2.5,
    borderBottomColor: colors.blue
  },
  dropdown1BtnTxtStyle: {
    color: colors.blue,
    fontSize: 14,
    textAlign: 'left',
  },
  dropdown1DropdownStyle: {
    backgroundColor: '#EFEFEF'
  },
  dropdown1RowStyle: {
    backgroundColor: '#EFEFEF',
    borderBottomColor: colors.blue
  },
  dropdown1RowTxtStyle: {
    color: colors.blue,
    textAlign: 'left'
  },
  dropdown1SelectedRowStyle: {
    backgroundColor: 'rgba(0,0,0,0.1)'
  },
  dropdown1searchInputStyleStyle: {
    backgroundColor: '#EFEFEF',
    borderRadius: 8,
    borderBottomWidth: 1,
    borderBottomColor: '#444',
  }
});

export { colors, sizes, fonts, styles };
